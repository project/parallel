Parallel module for Drupal 6
==================================

Installation
------------

 1) Copy the Parallel module to sites/all/modules.

 2) Enable it in admin/build/modules.